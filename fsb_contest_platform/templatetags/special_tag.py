from django import template
register = template.Library()
def cut_bio(value):
    return value[:120]
register.filter('cut_bio',cut_bio)

@register.filter
def filename(value):
    return value.name.split('/')[-1]
