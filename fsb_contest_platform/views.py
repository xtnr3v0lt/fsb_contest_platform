from django.shortcuts import render_to_response #This to render template to requests
from django.http import HttpResponseRedirect,HttpResponseForbidden #This is to redirect the user to the right page
from django.contrib import auth #package to check if the user is allready logged in or not also control the authentification
from django.core.context_processors import csrf #This to verify the CSRF security issue (protection against CSRF attach)
from django.shortcuts import render
from SiteSetting.models import *
from players.models import *
from task_manager.models import *
from django.contrib.auth.decorators import login_required


@login_required(login_url='/ctf/login/')
def home(request):
    c = {}
    site = WebsiteSetting.objects.all()
    if site:
        c['site'] = site[0]
    else:
        HttpResponseRedirect('/admin/?msg=Configure_site_setting')
    c.update(csrf(request))
    return render(request,'home.html',c)



def ctf_login(request):
	c = {}
	c.update(csrf(request))

	if request.POST:
		username = request.POST.get("username","")
		if username == "":
			c['error'] = "username field is required"
		else:
			password = request.POST.get("password","")
			if password == "":
				c['error'] = "password field is required"
			else:
				user_with_username = auth.authenticate(username=username,password=password)
				if user_with_username:
					if user_with_username.is_active:
						auth.login(request,user_with_username)
						return HttpResponseRedirect('/ctf/profile/')
					else:
						c['error'] = "Sorry but login/password are invalide please contact the Administrator!"
	return render(request,'login.html',c)

@login_required(login_url='/ctf/login/')
def logout(request):
	auth.logout(request)
	return HttpResponseRedirect("/")

def ctf_register(request):
    c = {}
    c.update(csrf(request))
    return render(request,'ctf_register.html',c)



@login_required(login_url='/ctf/login/')
def ctf_profile(request):
    c = {}
    c.update(csrf(request))
    team = Team.objects.filter(user=request.user)
    if team:
        c['team'] = team[0]
        members = Player.objects.filter(team=team[0])
        c['members'] = members
    else:
        return HttpResponseRedirect('/ctf/login/')
    return render(request,'ctf_profile.html',c)



@login_required(login_url='/ctf/login/')
def ctf_teams(request):
    c = {}
    c.update(csrf(request))
    c['teams'] = Team.objects.all()
    return render(request,'ctf_teams.html',c)


@login_required(login_url='/ctf/login/')
def ctf_scoreboard(request):
    c = {}
    c.update(csrf(request))
    return render(request,'ctf_scoreboard.html',c)

@login_required(login_url='/ctf/login/')
def ctf_open_qcm(request,qcm_id=1):
    c = {}
    c.update(csrf(request))
    qcm = qcm_task.objects.filter(pk=qcm_id)
    if qcm:
        c['qcm'] = qcm[0]
        answered_before = qcm_answer.objects.filter(user=request.user,qcm=qcm[0])
        if answered_before:
            c['answered_before'] = answered_before[0]
    else:
        return HttpResponseRedirect('/ctf/qcm/')
    if request.POST:
        qcm = qcm[0]
        if 'answered_before' in c:
            return HttpResponseRedirect('/ctf/open/qcm/'+str(qcm.id))
        else:
            answer_a_is_valid = request.POST.get('answer_a') == 'on'
            answer_b_is_valid = request.POST.get('answer_b') == 'on'
            answer_c_is_valid = request.POST.get('answer_c') == 'on'
            is_valide = (qcm.answer_a_is_valid == answer_a_is_valid  and qcm.answer_b_is_valid == answer_b_is_valid and qcm.answer_c_is_valid == answer_c_is_valid)
            answer = qcm_answer(user=request.user,qcm=qcm,answer_a_is_valid=answer_a_is_valid,answer_b_is_valid=answer_b_is_valid,answer_c_is_valid=answer_c_is_valid,is_valide=is_valide)
            try:
                answer.save()
                c['answered_before'] = answer
                if is_valide:
                    c['music'] = 'happy'
                else:
                    c['music'] = 'sad'
            except:
                return HttpResponseRedirect('cannot save the answer contact admin')
    return render(request,'ctf_open_qcm.html',c)
@login_required(login_url='/ctf/login/')
def ctf_open_problem(request,problem_id=1):
    c = {}
    c.update(csrf(request))
    problem = problem_tasks.objects.filter(pk=problem_id)
    if problem:
        c['problem'] = problem[0]
        c['count'] = len(problem_tasks_submit.objects.filter(problem=problem[0]))
        submited_before = problem_tasks_submit.objects.filter(user=request.user,problem=problem[0])
        if submited_before:
            c['submited_before']  = submited_before[0]
    else:
        return HttpResponseRedirect('/ctf/profile/')

    if request.POST:
        if 'submited_before' in c:
            return HttpResponseForbidden('You have already submited a file')
        else:
            filename = request.FILES['file']
            answer = problem_tasks_submit(user=request.user,problem=c['problem'],submited_file=filename)
            try:
                answer.save()
            except:
                return HttpResponseForbidden('Error While uploading file contact admin')
    return render(request,'ctf_open_problem.html',c)
@login_required(login_url='/ctf/login/')
def ctf_tasks_qcm(request):
    c = {}
    c.update(csrf(request))
    ex_tasks = qcm_answer.objects.values_list('qcm', flat=True).filter(user=request.user)
    tasks = qcm_task.objects.exclude(pk__in=set(ex_tasks))
    c['ex_tasks'] =  qcm_task.objects.filter(pk__in=set(ex_tasks))
    c['tasks'] = tasks
    return render(request,'ctf_qcm.html',c)

@login_required(login_url='/ctf/login/')
def ctf_tasks_problem(request):
    c = {}
    c.update(csrf(request))
    c['problems'] = problem_tasks.objects.all()
    return render(request,'ctf_problem.html',c)




def error_203(request):
    c = {}
    c.update(csrf(request))
    return render(request,'error_203.html',c)
