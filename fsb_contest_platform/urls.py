"""fsb_contest_platform URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import patterns,include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','fsb_contest_platform.views.home'),
    url(r'^ctf/login/$','fsb_contest_platform.views.ctf_login'),
    url(r'^ctf/register/$','fsb_contest_platform.views.ctf_register'),
    url(r'^ctf/profile/$','fsb_contest_platform.views.ctf_profile'),
    url(r'^ctf/teams/$','fsb_contest_platform.views.ctf_teams'),
    url(r'^ctf/scoreboard/$','fsb_contest_platform.views.ctf_scoreboard'),
    url(r'^ctf/qcm/$','fsb_contest_platform.views.ctf_tasks_qcm'),
    url(r'^ctf/open/qcm/(?P<qcm_id>\d+)/$','fsb_contest_platform.views.ctf_open_qcm'),
    url(r'^ctf/problem/$','fsb_contest_platform.views.ctf_tasks_problem'),
    url(r'^ctf/open/problem/(?P<problem_id>\d+)/$','fsb_contest_platform.views.ctf_open_problem'),
    url(r'^logout/$','fsb_contest_platform.views.logout'),
    url(r'^error/203/$','fsb_contest_platform.views.error_203')
)+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
