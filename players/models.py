from django.db import models
from django.contrib.auth.models import User
from uuid import  uuid4
import os
def path_and_rename(path, prefix):
    def wrapper(instance, filename):
        ext = filename.split('.')[-1]
        project = "pid_%s" % (instance.id,)
        # get filename
        if instance.pk:
            complaint_id = "cid_%s" % (instance.pk,)
            filename = '{}.{}.{}.{}'.format(prefix, project, complaint_id, ext)
        else:
            # set filename as random string
            random_id = "rid_%s" % (uuid4().hex,)
            filename = '{}.{}.{}.{}'.format(prefix, project, random_id, ext)
            # return the whole path to the file
        return os.path.join(path, filename)
    return wrapper


# Create your models here.
class Team(models.Model):
    choices = (('1','1er'),('2','2eme'),('3','3eme'),('o','Others'))
    user = models.ForeignKey(User)
    team_name = models.CharField(max_length=255)
    niveau = models.CharField(max_length=1,choices=choices)
    team_biographie = models.TextField()
    pic = models.FileField(upload_to=path_and_rename('teams/','team'))
    class Meta:
        verbose_name = "Team"
        verbose_name_plural = "Teams"
    def __str__(self):
        return self.team_name
class Player(models.Model):
    team = models.ForeignKey(Team)
    nom = models.CharField(max_length=255)
    prenom = models.CharField(max_length=255)
    cin = models.IntegerField()
    classe = models.CharField(max_length=255,default='SI1A')
    email = models.CharField(max_length=255,default='anonymous@firefox.com')
    birth_date = models.DateTimeField()
    pic = models.FileField(upload_to=path_and_rename('profile','pic_'),default='profile/none.jpg')
    class Meta :
        verbose_name = 'Player'
        verbose_name_plural = 'Players'
    def __str__(self):
        return self.nom+" "+self.prenom
