from django.contrib import admin
from . import models
# Register your models here.
class TeamAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ('team_name','user',)
    search_fields =  ['team_name',]
class PlayerAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ('nom','prenom','team')
    search_fields = ['nom','prenom',]
admin.site.register(models.Team,TeamAdmin)
admin.site.register(models.Player,PlayerAdmin)
