from django.contrib import admin
from . import models
# Register your models here.
class qcm_task_admin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ('question','answer_a_is_valid','answer_b_is_valid','answer_c_is_valid','time_post')
    search_fields = ['question',]

class qcm_answer_admin(admin.ModelAdmin):
    list_display = ('user','qcm','answer_a_is_valid','answer_b_is_valid','answer_c_is_valid','time_answer')
    list_per_page = 10
    search_fields = ['qcm',]

class problem_tasks_admin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ('title','category','level','score')
    search_fields = ['title',]

class problem_tasks_submit_admin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ('user','problem','submited_file')
    search_fields = ['user','problem']
admin.site.register(models.qcm_task,qcm_task_admin)
admin.site.register(models.qcm_answer, qcm_answer_admin)
admin.site.register(models.problem_tasks,problem_tasks_admin)
admin.site.register(models.problem_tasks_submit,problem_tasks_submit_admin)
