from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class qcm_task(models.Model):
    question = models.CharField(max_length=1000,default="Type your question here!")
    score = models.IntegerField()
    answer_a = models.CharField(max_length=100,default='Answer A')
    answer_a_is_valid = models.BooleanField(default=False)
    answer_b = models.CharField(max_length=100,default='Answer B')
    answer_b_is_valid = models.BooleanField(default=False)
    answer_c = models.CharField(max_length=100,default='Answer C')
    answer_c_is_valid = models.BooleanField(default=False)
    time_post = models.DateTimeField(auto_now_add = True)
    description = models.TextField(default='')
    class Meta:
        verbose_name = "Question"
        verbose_name_plural = "Questions"
    def __str__(self):
        return self.question
class qcm_answer(models.Model):
    user = models.ForeignKey(User)
    qcm = models.ForeignKey(qcm_task)
    answer_a_is_valid = models.BooleanField(default=False)
    answer_b_is_valid = models.BooleanField(default=False)
    answer_c_is_valid = models.BooleanField(default=False)
    time_answer = models.DateTimeField(auto_now_add = True)
    is_valide=models.BooleanField(default=False)
    class Meta:
        verbose_name = "Question Answer"
        verbose_name_plural = "Question Answers"
    def __str__(self):
        return self.user.username+" - "+self.qcm.question


class problem_tasks(models.Model):
    level = models.CharField(max_length=255,help_text="Use (Easy,Medium,Hard,Professional)")
    score = models.IntegerField()
    category = models.CharField(max_length=255,help_text="Use (Word,PowerPoint,Excel)")
    title = models.CharField(max_length=400,default="Problem Title")
    description = models.TextField()
    time_post = models.DateTimeField(auto_now_add=True)
    attachment = models.FileField(upload_to="problem/attachment/",help_text="Upload .zip file if you want to share multiple file")
    class Meta:
        verbose_name = "Problem"
        verbose_name_plural = "Problems"
    def __str__(self):
        return self.title

class problem_tasks_submit(models.Model):
    problem = models.ForeignKey(problem_tasks)
    user = models.ForeignKey(User)
    submited_file = models.FileField(upload_to="problem/submit/file/")
    is_valide = models.BooleanField(default=False)
    score = models.IntegerField(default=0)
    answer_time = models.DateTimeField(auto_now_add=True)
    class Meta:
        verbose_name = "Problem Answer"
        verbose_name_plural="Problem Answers"
    def __str__(self):
        return self.problem.title
