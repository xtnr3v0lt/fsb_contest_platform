# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0002_problem_tasks'),
    ]

    operations = [
        migrations.AddField(
            model_name='problem_tasks',
            name='attachment',
            field=models.FileField(default='none', help_text=b'Upload .zip file if you want to share multiple file', upload_to=b'problem/attachment/'),
            preserve_default=False,
        ),
    ]
