# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0005_qcm_answer_is_valide'),
    ]

    operations = [
        migrations.AddField(
            model_name='qcm_task',
            name='description',
            field=models.TextField(default=b''),
        ),
    ]
