# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('task_manager', '0003_problem_tasks_attachment'),
    ]

    operations = [
        migrations.CreateModel(
            name='problem_tasks_submit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('submited_file', models.FileField(upload_to=b'problem/submit/file/')),
                ('is_valide', models.BooleanField(default=False)),
                ('score', models.IntegerField(default=0)),
                ('answer_time', models.DateTimeField(auto_now_add=True)),
                ('problem', models.ForeignKey(to='task_manager.problem_tasks')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Problem Answer',
                'verbose_name_plural': 'Problem Answers',
            },
        ),
        migrations.AlterModelOptions(
            name='qcm_answer',
            options={'verbose_name': 'Question Answer', 'verbose_name_plural': 'Question Answers'},
        ),
    ]
