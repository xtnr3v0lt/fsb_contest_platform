# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='qcm_answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer_a_is_valid', models.BooleanField(default=False)),
                ('answer_b_is_valid', models.BooleanField(default=False)),
                ('answer_c_is_valid', models.BooleanField(default=False)),
                ('time_answer', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Answer',
                'verbose_name_plural': 'Answers',
            },
        ),
        migrations.CreateModel(
            name='qcm_task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(default=b'Type your question here!', max_length=1000)),
                ('score', models.IntegerField()),
                ('answer_a', models.CharField(default=b'Answer A', max_length=100)),
                ('answer_a_is_valid', models.BooleanField(default=False)),
                ('answer_b', models.CharField(default=b'Answer B', max_length=100)),
                ('answer_b_is_valid', models.BooleanField(default=False)),
                ('answer_c', models.CharField(default=b'Answer C', max_length=100)),
                ('answer_c_is_valid', models.BooleanField(default=False)),
                ('time_post', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Question',
                'verbose_name_plural': 'Questions',
            },
        ),
        migrations.AddField(
            model_name='qcm_answer',
            name='qcm',
            field=models.ForeignKey(to='task_manager.qcm_task'),
        ),
        migrations.AddField(
            model_name='qcm_answer',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
