# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0004_auto_20151209_1257'),
    ]

    operations = [
        migrations.AddField(
            model_name='qcm_answer',
            name='is_valide',
            field=models.BooleanField(default=False),
        ),
    ]
