# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='problem_tasks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.CharField(help_text=b'Use (Easy,Medium,Hard,Professional)', max_length=255)),
                ('score', models.IntegerField()),
                ('category', models.CharField(help_text=b'Use (Word,PowerPoint,Excel)', max_length=255)),
                ('title', models.CharField(default=b'Problem Title', max_length=400)),
                ('description', models.TextField()),
                ('time_post', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Problem',
                'verbose_name_plural': 'Problems',
            },
        ),
    ]
