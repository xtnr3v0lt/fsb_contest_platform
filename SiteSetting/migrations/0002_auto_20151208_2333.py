# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('SiteSetting', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='websitesetting',
            name='contact_image',
            field=models.FileField(default=1, upload_to=b'site/setting/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='email',
            field=models.CharField(default=b'fpt.informatique2016@gmail.com', max_length=255),
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='facebook',
            field=models.CharField(default=b'https://web.facebook.com/formation.pour.tous1/', max_length=255),
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='intro_quote',
            field=models.CharField(default=b'with the best of the best you become the best', max_length=255),
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='intro_title',
            field=models.CharField(default=b'C2IFication', max_length=255),
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='item_one_description',
            field=models.TextField(default=b'This is Description for Item One'),
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='item_one_image',
            field=models.FileField(default=1, upload_to=b'site/setting/formation/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='item_one_title',
            field=models.CharField(default=b'Item One', max_length=255),
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='item_three_description',
            field=models.TextField(default=b'This is Description for Item three'),
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='item_three_image',
            field=models.FileField(default=1, upload_to=b'site/setting/formation/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='item_three_title',
            field=models.CharField(default=b'Item three', max_length=255),
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='item_two_description',
            field=models.TextField(default=b'This is Description for Item two'),
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='item_two_image',
            field=models.FileField(default=datetime.datetime(2015, 12, 8, 23, 33, 36, 586152, tzinfo=utc), upload_to=b'site/setting/formation/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='item_two_title',
            field=models.CharField(default=b'Item two', max_length=255),
        ),
        migrations.AddField(
            model_name='websitesetting',
            name='twitter',
            field=models.CharField(default=b'https://twitter.com/fptinformatique', max_length=255),
        ),
    ]
