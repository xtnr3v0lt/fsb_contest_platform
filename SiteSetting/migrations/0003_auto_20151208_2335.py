# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SiteSetting', '0002_auto_20151208_2333'),
    ]

    operations = [
        migrations.AlterField(
            model_name='websitesetting',
            name='contact_image',
            field=models.FileField(default=b'site/setting/contact-bg.jpg', upload_to=b'site/setting/'),
        ),
        migrations.AlterField(
            model_name='websitesetting',
            name='intro_image',
            field=models.FileField(default=b'site/setting/bg.jpg', upload_to=b'site/setting/'),
        ),
        migrations.AlterField(
            model_name='websitesetting',
            name='item_one_image',
            field=models.FileField(default=b'site/setting/formation/item1.png', upload_to=b'site/setting/formation/'),
        ),
        migrations.AlterField(
            model_name='websitesetting',
            name='item_three_image',
            field=models.FileField(default=b'site/setting/formation/item3.png', upload_to=b'site/setting/formation/'),
        ),
        migrations.AlterField(
            model_name='websitesetting',
            name='item_two_image',
            field=models.FileField(default=b'site/setting/formation/item2.png', upload_to=b'site/setting/formation/'),
        ),
    ]
