# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='WebsiteSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'FPT Website Platform', max_length=255)),
                ('intro_image', models.FileField(upload_to=b'site/setting/')),
            ],
            options={
                'verbose_name': 'Site Setting',
                'verbose_name_plural': 'Site Settins',
            },
        ),
    ]
