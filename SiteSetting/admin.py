from django.contrib import admin
from . import models
# Register your models here.
class WebsiteSettingAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ('title',)
    search_field = ['title',]

admin.site.register(models.WebsiteSetting , WebsiteSettingAdmin)
