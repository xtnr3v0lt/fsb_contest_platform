from django.db import models

# Create your models here.
class WebsiteSetting(models.Model):
    title = models.CharField(max_length=255,default="FPT Website Platform")
    intro_image = models.FileField(upload_to='site/setting/',default='site/setting/bg.jpg')
    intro_title = models.CharField(max_length=255,default="C2IFication")
    intro_quote = models.CharField(max_length=255,default="with the best of the best you become the best")
    item_one_image = models.FileField(upload_to='site/setting/formation/',default='site/setting/formation/item1.png')
    item_one_title = models.CharField(max_length=255,default="Item One")
    item_one_description = models.TextField(default="This is Description for Item One")
    item_two_image = models.FileField(upload_to='site/setting/formation/',default='site/setting/formation/item2.png')
    item_two_title = models.CharField(max_length=255,default="Item two")
    item_two_description = models.TextField(default="This is Description for Item two")
    item_three_image = models.FileField(upload_to='site/setting/formation/',default='site/setting/formation/item3.png')
    item_three_title = models.CharField(max_length=255,default="Item three")
    item_three_description = models.TextField(default="This is Description for Item three")
    contact_image = models.FileField(upload_to='site/setting/',default='site/setting/contact-bg.jpg')
    facebook = models.CharField(max_length=255,default='https://web.facebook.com/formation.pour.tous1/')
    twitter = models.CharField(max_length=255,default='https://twitter.com/fptinformatique')
    email = models.CharField(max_length=255,default='fpt.informatique2016@gmail.com')
    class Meta:
        verbose_name = "Site Setting"
        verbose_name_plural = "Site Settins"
